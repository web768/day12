<?php
session_start();

$gender = array("Nam", "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

function is_format_date($date, $format = 'd/m/Y')
{
    $_date = DateTime::createFromFormat($format, $date);
    return $_date && $_date->format($format) === $date;
}

$errors = array();

if (!empty($_POST)) {
    
    if (($_POST["user_name"]) == '') {
        $errors[] = "Hãy nhập tên";
    } else {
        $_SESSION['user']['name'] = $_POST['user_name'];
    }
    
    if (!isset($_POST["gender"])) {
        $errors[] = "Hãy chọn giới tính";
    } else {
        $_SESSION['user']['gender'] = $_POST['gender'];
    }
    
    if (($_POST["department"]) == '') {
        $errors[] = "Hãy chọn phân khoa";
    } else {
        $_SESSION['user']['department'] = $_POST['department'];
    }
    
    if (($_POST["birthday"]) == '') {
        $errors[] = "Hãy nhập ngày sinh";
    } elseif (!is_format_date($_POST["birthday"])) {
        $errors[] = "Ngày sinh không đúng định dạng";
    } else {
        $_SESSION['user']['birthday'] = $_POST['birthday'];
    }

    $upload_dir = "uploads/";

    if (!file_exists($upload_dir)) {
        mkdir($upload_dir, 0777, true);
    }
    $img = $_FILES['image']['name'];

    // get ext of file
    $imageFileType = strtolower(pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));

    // get name of file
    $imgName = basename($_FILES["image"]["name"], "." . $imageFileType);
    $file = $_FILES['image']['tmp_name'];

    // make new image name
    $target_file = $upload_dir . $imgName . "_" . date("YmdHis") . "." . $imageFileType;

    if (($_FILES['image']) == '') {
        $errors[] = "Hãy chọn ảnh";
    } elseif ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $errors[] = "Chỉ được upload file JPG, JPEG, PNG";
    }else{
        if (isset($_POST["submit"]) && isset($file)){
            //  check real image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
    
            if($check == false) {
                $errors[] = "File đã chọn không phải ảnh";
            }else {
                move_uploaded_file($file, $target_file);
            }
        }
    }




    $_SESSION['user']['address'] = $_POST['address'];

    $_SESSION['user']['image'] = $target_file;

    if (!$errors && count($_SESSION['user']) == 6) {
        header("Location: confirm.php");
    }
}

?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <link rel="stylesheet" href="styles.css">

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>Register</title>
</head>

<body>
<div class="register">
    <?php
    foreach ($errors as $error) {
        echo "<div class='alert alert-danger' role='alert'>$error</div>";
    }
    ?>
    <div class="register-infor">
        <form action="" enctype="multipart/form-data" method="post">
            <div class="input-box">
                <label for="" class="text-label">Họ và tên<span class="text-danger">*</span></label>
                <input type="text" class="text-field" name="user_name">
            </div>
            <div class="input-box">
                <label for="" class=" text-label">Giới tính<span class="text-danger">*</span></label>
                <?php
                for ($i = 0; $i < count($gender); $i++) {
                    echo "<input type='radio' name='gender' value=$i><label class='radio-label' style='color: black'>$gender[$i]</label>";
                }
                ?>
            </div>
            <div class="input-box">
                <label for="department" class="text-label">Phân khoa<span class="text-danger">*</span></label>
                <select name="department" id="department" class="select-field">
                    <option value=''>Chọn phân khoa</option>
                    <?php
                    foreach ($department as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="input-box">
                <label for="" class="text-label">Ngày sinh<span class="text-danger">*</span></label>
                <div class='input-group' id='date-picker'>
                    <input type='text' class="form-control" placeholder="dd/mm/yyyy" name="birthday"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </span>
                </div>
            </div>
            <div class="input-box">
                <label for="" class="text-label">Địa chỉ</label>
                <input type="text" class="text-field" name="address">
            </div>
            <div class="input-box" style="display: flex;">
                <label for="" class="text-label">Hình ảnh</label>
                <input type="file" id="img" class="image-field" name="image">
            </div>
            <div class="btn">
                <button class="btn-submit" type="submit" name="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</div>
<!-- script -->
<script>
    $(function () {
        $('#date-picker').datetimepicker({
            format: 'DD/MM/YYYY',
        })
    });
</script>
<!-- end script -->
</body>
</html>

